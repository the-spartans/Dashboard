import Vue from 'vue'
import Router from 'vue-router'
import store from './../store'

import Login from '@/components/page/Login'
import NotFound from '@/components/page/NotFound'

import DashboardLayout from '@/components/layout/Dashboard'
import EmptyLayout from '@/components/layout/Empty'

// Dashboard
import Dashboard from '@/components/pages/Dashboard/Main'

// Schedule
import ScheduleOverview from '@/components/pages/Schedule/Overview'
// Notifications
import NotificationsOverview from '@/components/pages/Notifications/Overview'
import NotificationsDetail from '@/components/pages/Notifications/Detail'
// Rooms
import RoomsOverview from '@/components/pages/Rooms/Overview'
import RoomsDetail from '@/components/pages/Rooms/Detail'
// Calendar
import CalendarOverview from '@/components/pages/Calendar/Overview'
import CalendarDetail from '@/components/pages/Calendar/Detail'

Vue.use(Router)

const router = new Router({
  routes: [
    {
      path: '/login',
      name: 'Login',
      component: Login,
      meta: { requiresAuth: false }
    },
    {
      path: '/',
      component: DashboardLayout,
      meta: { requiresAuth: true },
      children: [
        {
          path: '/',
          name: 'Demo',
          component: Dashboard,
          meta: { requiresAuth: true }
        },

        // Schedule
        {
          path: '/schedule',
          component: EmptyLayout,
          meta: { requiresAuth: true },
          children: [
            {path: '', name: 'ScheduleOverview', component: ScheduleOverview, meta: { requiresAuth: true }}
          ]
        },

        // Notifications
        {
          path: '/notifications',
          component: EmptyLayout,
          meta: { requiresAuth: true },
          children: [
            {path: '', name: 'NotificationsOverview', component: NotificationsOverview, meta: { requiresAuth: true }},
            {path: 'new', name: 'NotificationsCreate', component: NotificationsDetail, meta: { requiresAuth: true }},
            {path: ':id', name: 'NotificationsDetails', component: NotificationsDetail, meta: { requiresAuth: true }}
          ]
        },

        // Rooms
        {
          path: '/rooms',
          component: EmptyLayout,
          meta: { requiresAuth: true },
          children: [
            {path: '', name: 'RoomsOverview', component: RoomsOverview, meta: { requiresAuth: true }},
            {path: 'new', name: 'RoomsCreate', component: RoomsDetail, meta: { requiresAuth: true }},
            {path: ':id', name: 'RoomsDetails', component: RoomsDetail, meta: { requiresAuth: true }}
          ]
        },

        // Calendar
        {
          path: '/calendar',
          component: EmptyLayout,
          meta: { requiresAuth: true },
          children: [
            {path: '', name: 'CalendarOverview', component: CalendarOverview, meta: { requiresAuth: true }},
            {path: 'new', name: 'CalendarCreate', component: CalendarDetail, meta: { requiresAuth: true }},
            {path: ':id', name: 'CalendarDetails', component: CalendarDetail, meta: { requiresAuth: true }}
          ]
        }
      ]
    },
    {
      path: '*',
      name: 'NotFound',
      component: NotFound,
      meta: { requiresAuth: false }
    }
  ]
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth) && !store.state.auth.authenticated) {
    next({
      name: 'Login',
      query: { redirect: to.fullPath }
    })
  } else {
    next()
  }
})

export default (router)
